name = "djv"
version = "1.0.5"
authors = ["DJV imaging"]
description = "DJV Imaging provides professional movie playback software for film production"
tools = ["djv"]
# requires = ["python"]
# help = "file://{root}/help.html"
uuid = "acf9ece0-a470-4bd8-b346-999f2060d50e"
def commands():
    # env.PYTHONPATH.append("{root}/python")
    env.PATH.append("$BD_SOFTS/djv-view/{version}/bin")
    env.LD_LIBRARY_PATH.append("$BD_SOFTS/djv-view/{version}/lib")
